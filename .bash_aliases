## Aliases

alias aliases="nano ~/.bash_aliases && . ~/.bash_aliases";
# alias aliases=' nano "/c/Program Files/Git/etc/profile.d/aliases.sh"; . nano "/c/Program Files/Git/etc/profile.d/aliases.sh"' # Useful for 'Git for Windows'
alias cls="printf '\033c\e[3J'";

## Functions